#include "catch.hpp"

#include "../src/salsa20_detail.hpp"


namespace mcp {
namespace salsa20 {
namespace test {

using namespace mcp::salsa20::detail;

TEST_CASE("quarterround", "[internals]") {
	CHECK(quarterround(1,0,0,0) == std::make_tuple(0x08008145, 0x00000080, 0x00010200, 0x20500000));
}

TEST_CASE("rowround", "[internals]") {
	const auto expected = word_block_16{
		0x08008145, 0x00000080, 0x00010200, 0x20500000,
		0x20100001, 0x00048044, 0x00000080, 0x00010000,
		0x00000001, 0x00002000, 0x80040000, 0x00000000,
		0x00000001, 0x00000200, 0x00402000, 0x88000100
	};
	CHECK(rowround({1,0,0,0,  1,0,0,0,  1,0,0,0,  1,0,0,0}) == expected);
}

TEST_CASE("columnround", "[internals]") {
	const auto expected = word_block_16{
		0x10090288, 0x00000000, 0x00000000, 0x00000000,
		0x00000101, 0x00000000, 0x00000000, 0x00000000,
		0x00020401, 0x00000000, 0x00000000, 0x00000000,
		0x40a04001, 0x00000000, 0x00000000, 0x00000000
	};
	CHECK(columnround({1,0,0,0,  1,0,0,0,  1,0,0,0,  1,0,0,0}) == expected);
}

TEST_CASE("doubleround", "[internals]") {
	const auto expected_1 = word_block_16{
		0x8186a22d, 0x0040a284, 0x82479210, 0x06929051,
		0x08000090, 0x02402200, 0x00004000, 0x00800000,
		0x00010200, 0x20400000, 0x08008104, 0x00000000,
		0x20500000, 0xa0000040, 0x0008180a, 0x612a8020
	};
	CHECK(doubleround({1,0,0,0,  0,0,0,0,  0,0,0,0,  0,0,0,0}) == expected_1);

	const auto expected_2 = word_block_16{
		0xccaaf672, 0x23d960f7, 0x9153e63a, 0xcd9a60d0,
		0x50440492, 0xf07cad19, 0xae344aa0, 0xdf4cfdfc,
		0xca531c29, 0x8e7943db, 0xac1680cd, 0xd503ca00,
		0xa74b2ad6, 0xbc331c5c, 0x1dda24c7, 0xee928277
	};
	const auto input_2 = word_block_16{
		0xde501066, 0x6f9eb8f7, 0xe4fbbd9b, 0x454e3f57,
		0xb75540d3, 0x43e93a4c, 0x3a6f2aa0, 0x726d6b36,
		0x9243f484, 0x9145d1e8, 0x4fa9d247, 0xdc8dee11,
		0x054bf545, 0x254dd653, 0xd9421b6d, 0x67b276c1
	};
	CHECK(doubleround(input_2) == expected_2);
}

TEST_CASE("inv_little_endian", "[internals]") {
	CHECK(inv_little_endian<0>(0x091e4b56) == 86);
	CHECK(inv_little_endian<1>(0x091e4b56) == 75);
	CHECK(inv_little_endian<2>(0x091e4b56) == 30);
	CHECK(inv_little_endian<3>(0x091e4b56) ==  9);
}

TEST_CASE("salsa20_hash", "[internals]") {
	const auto input_1     = byte_block_64{}; // all zeroes
	const auto expected_1 = byte_block_64{}; // all zeroes
	CHECK(salsa20_hash(input_1) == expected_1);

	const auto input_2 = byte_block_64{
		211,159, 13,115, 76, 55, 82,  183, 3,   117, 222,  37, 191, 187, 234, 136,
		49, 237,179, 48, 1,  106,178, 219, 175, 199, 166,  48,  86,  16, 179, 207,
		31, 240, 32, 63, 15, 83, 93,  161, 116, 147,  48, 113, 238,  55, 204,  36,
		79, 201,235, 79, 3,  81, 156, 47,  203,  26, 244, 243,  88, 118, 104,  54
	};
	const auto expected_2 = byte_block_64{
		109, 42,178,168,156,240,248,238,168,196,190,203, 26,110,170,154,
		29, 29,150, 26,150, 30,235,249,190,163,251, 48, 69,144, 51, 57,
		118, 40,152,157,180, 57, 27, 94,107, 42,236, 35, 27,111,114,114,
		219,236,232,135,111,155,110, 18, 24,232, 95,158,179, 19, 48,202
	};
	CHECK(salsa20_hash(input_2) == expected_2);
}

TEST_CASE("salsa20_expand [128]", "[primitives]") {
	const auto k = byte_block_16{
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
	};
	const auto n = byte_block_16{
		101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116
	};
	const auto expected = byte_block_64{
		39,173, 46,248, 30,200, 82, 17, 48, 67,254,239, 37, 18, 13,247,
		241,200, 61,144, 10, 55, 50,185, 6, 47,246,253,143, 86,187,225,
		134, 85,110,246,161,163, 43,235,231, 94,171, 51,145,214,112, 29,
		14,232, 5, 16,151,140,183,141,171, 9,122,181,104,182,177,193
	};

	CHECK(salsa20_expand(k, n) == expected);
}

TEST_CASE("salsa20_expand [256]", "[primitives]") {
	const auto k = byte_block_32{
		  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16,
		201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216
	};
	const auto n = byte_block_16{
		101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116
	};
	const auto expected = byte_block_64{
		69, 37, 68, 39, 41, 15,107,193,255,139,122, 6,170,233,217, 98,
		89,144,182,106, 21, 51,200, 65,239, 49,222, 34,215,114, 40,126,
		104,197, 7,225,197,153, 31, 2,102, 78, 76,176, 84,245,246,184,
		177,160,133,130, 6, 72,149,119,192,195,132,236,234,103,246, 74
	};

	CHECK(salsa20_expand(k, n) == expected);
}

} // namespace test
} // namespace salsa20
} // namespace mcp

