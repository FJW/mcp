#include "catch.hpp"

#include "../src/sha2_256_detail.hpp"

namespace mcp {
namespace sha2 {
namespace test {

using namespace mcp::sha2::detail;


TEST_CASE("sha2_256-compression", "[internals][sha2]") {
	auto state = word_block_8{{1,2,3,4,5,6,7,8}};
	compression_256(state, 11, 20);
	CHECK(state == (word_block_8{{1420297904,1,2,3,346030769,5,6,7}}));
}


TEST_CASE("sha2_256-compression*16", "[internals][sha2]") {
	auto padded_abc = word_block_16{{
		0x61626380, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000018
	}};
	const auto expected = word_block_8{{
		0xb0fa238e, 0xc0645fde, 0xd932eb16, 0x87912990, 0x07590dcd, 0x0b92f20c, 0x745a48de, 0x1e578218
	}};
	auto state = init_256;
	for (auto i = 0u; i < 16u; ++i) {
		compression_256(state, k_256[i], padded_abc[i]);
	}
	CHECK(state == expected);
}


TEST_CASE("sha2_256-nextw", "[internals][sha2]") {
	auto w = word_block_16{{
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
	}};
	const auto expected = word_block_16{{
		2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,67559435
	}};
	next_w(w);
	CHECK(w == expected);
}


TEST_CASE("sha2_256-attach-block", "[internals][sha2]") {
	const auto padded_abc = word_block_16{{
		0x61626380, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000018
	}};
	const auto expected = word_block_8{{
		0xba7816bf, 0x8f01cfea, 0x414140de, 0x5dae2223, 0xb00361a3, 0x96177a9c, 0xb410ff61, 0xf20015ad
	}};
	auto state = init_256;
	attach_block(state, padded_abc);
	CHECK(state == expected);
}


TEST_CASE("sha2_256-shiffted_end_byte", "[internals][sha2]") {
	CHECK(shiffted_end_byte(0) == 0x80000000u);
}


TEST_CASE("sha2_256-padding1", "[internals][sha2]") {
	const auto str = "abc";
	const auto expected = word_block_16{{
		0x61626380, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000018
	}};
	auto actual = word_block_16{};
	add_padding_1(actual, reinterpret_cast<const byte*>(str), 3);
	CHECK(actual == expected);
}

TEST_CASE("sha2_256-full_block", "[internals][sha2]") {
	const auto str = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
	const auto expected = word_block_8{{
		0x248d6a61, 0xd20638b8, 0xe5c02693, 0x0c3e6039, 0xa33ce459, 0x64ff2167, 0xf6ecedd4, 0x19db06c1
	}};
	const auto actual = sha2_256(reinterpret_cast<const byte*>(str), 56u);
	CHECK(actual == expected);
}


} // namespace test
} // namespace sha2;
} // namespace mcp

