#include "catch.hpp"

#include "../src/sha2_512_detail.hpp"

namespace mcp {
namespace sha2 {
namespace test {

using namespace mcp::sha2::detail;


TEST_CASE("sha2_512-compression*16", "[internals][sha2]") {
	auto padded_abc = dword_block_16{{
		0x6162638000000000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0x18
	}};
	const auto expected = dword_block_8{{
		0x0ae07c86b1181c75, 0xfa967eed85a08028, 0x44249631255d2ca0, 0x5e41214388186c14,
		0xa77b7c035dd4c161, 0x874bfe5f6aae9f2f, 0x860acf9effba6f61, 0xcdf3bff2883fc9d9
	}};
	auto state = init_512;
	for (auto i = 0u; i < 16u; ++i) {
		compression_512(state, k_512[i], padded_abc[i]);
	}
	CHECK(state == expected);
}


TEST_CASE("sha2_512-nextw", "[internals][sha2]") {
	auto w = dword_block_16{{
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
	}};
	const auto expected = dword_block_16{{
		2,3,4,5,6,7,8,9,10,11,12,13,14,15,16, 144642953657188484
	}};
	next_w(w);
	CHECK(w == expected);
}


TEST_CASE("sha2_512-attach-block", "[internals][sha2]") {
	const auto padded_abc = dword_block_16{{
		0x6162638000000000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0x18
	}};
	const auto expected = dword_block_8{{
		0xddaf35a193617aba, 0xcc417349ae204131, 0x12e6fa4e89a97ea2, 0x0a9eeee64b55d39a,
		0x2192992a274fc1a8, 0x36ba3c23a3feebbd, 0x454d4423643ce80e, 0x2a9ac94fa54ca49f
	}};
	auto state = init_512;
	attach_block(state, padded_abc);
	CHECK(state == expected);
}


TEST_CASE("sha2_512-padding1", "[internals][sha2]") {
	const auto str = "abc";
	const auto expected = dword_block_16{{
		0x6162638000000000, 0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0x18
	}};
	auto actual = dword_block_16{};
	add_padding_1(actual, reinterpret_cast<const byte*>(str), 3);
	CHECK(actual == expected);
}

TEST_CASE("sha2_512-full_block", "[internals][sha2]") {
	const auto str = "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmn"
		"hijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu";
	const auto expected = dword_block_8{{
		0x8e959b75dae313da, 0x8cf4f72814fc143f, 0x8f7779c6eb9f7fa1, 0x7299aeadb6889018,
		0x501d289e4900f7e4, 0x331b99dec4b5433a, 0xc7d329eeb6dd2654, 0x5e96e55b874be909
	}};
	const auto actual = sha2_512(reinterpret_cast<const byte*>(str), 112);
	CHECK(actual == expected);
}


} // namespace test
} // namespace sha2;
} // namespace mcp

