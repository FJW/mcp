#include "catch.hpp"

#include <vector>

#include "../src/utils.hpp"

namespace mcp {
namespace detail {
namespace test {

TEST_CASE("lrot_128", "[internals]") {
	const auto in = byte_block_16{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};

	const auto e1 = byte_block_16{2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1};
	const auto e2 = byte_block_16{3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2};
	const auto e3 = byte_block_16{8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7};
	const auto e4 = byte_block_16{10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9};
	const auto e5 = byte_block_16{15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14};
	const auto e6 = byte_block_16{16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

	CHECK(lrot<8>(in) == e1);
	CHECK(lrot<16>(in) == e2);
	CHECK(lrot<56>(in) == e3);
	CHECK(lrot<72>(in) == e4);
	CHECK(lrot<112>(in) == e5);
	CHECK(lrot<120>(in) == e6);
}

} // namespace test
} // namespace detail
} // namespace mcp

