#include "catch.hpp"

#include <vector>

#include "mcp/chacha20.hpp"


namespace mcp {
namespace chacha20 {
namespace test {

TEST_CASE("chacha20_128-basic-sanity", "[chacha20][API]") {
	const auto key = key_128{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
	const auto nounce = nounce_64{101,102,103,104,105,106,107,108};
	const auto m = std::string{"This is a textmessage that contains nothing interessting, nothing interessting at all!"};
	const auto c = highlevel::chacha20(key, nounce, m);
	CHECK(c.size() == m.size());
	CHECK(c != m);
	const auto dec = highlevel::chacha20(key, nounce, c);
	CHECK(dec == m);
}


TEST_CASE("chacha20_256_rfc_vectors", "[chacha20][API]") {
	const auto key = key_256{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	const auto nounce = nounce_64{0,0,0,0,0,0,0,0};
	const auto expected = std::array<byte,64>{{
		0x76, 0xb8, 0xe0, 0xad, 0xa0, 0xf1, 0x3d, 0x90, 0x40, 0x5d, 0x6a, 0xe5, 0x53, 0x86, 0xbd, 0x28,
		0xbd, 0xd2, 0x19, 0xb8, 0xa0, 0x8d, 0xed, 0x1a, 0xa8, 0x36, 0xef, 0xcc, 0x8b, 0x77, 0x0d, 0xc7,
		0xda, 0x41, 0x59, 0x7c, 0x51, 0x57, 0x48, 0x8d, 0x77, 0x24, 0xe0, 0x3f, 0xb8, 0xd8, 0x4a, 0x37,
		0x6a, 0x43, 0xb8, 0xf4, 0x15, 0x18, 0xa1, 0x1c, 0xc3, 0x87, 0xb6, 0x69, 0xb2, 0xee, 0x65, 0x86
	}};
	auto actual = std::array<byte,64>{{}};
	lowlevel::chacha20_stream(key, nounce, actual.data(), actual.size());
	CHECK(actual == expected);
}


TEST_CASE("chacha20_128_highlevel-basic-sanity", "[chacha20][API]") {
	const auto key = key_128{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
	const auto m = "Hello World!";
	const auto c1 = chacha20_enc(key, m);
	CHECK(m != c1);
	const auto c2 = chacha20_enc(key, m);
	CHECK(m != c2);
	// This may fail once during 2^64 tests, in other words never:
	CHECK(c1 != c2);
	CHECK(chacha20_dec(key, c1) == m);
	CHECK(chacha20_dec(key, c2) == m);
}


TEST_CASE("chacha20_256_highlevel-basic-sanity", "[chacha20][API]") {
	const auto key = key_256{
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
	const auto m = "Hello World!";
	const auto c1 = chacha20_enc(key, m);
	CHECK(m != c1);
	const auto c2 = chacha20_enc(key, m);
	CHECK(m != c2);
	// This may fail once during 2^64 tests, in other words never:
	CHECK(c1 != c2);
	CHECK(chacha20_dec(key, c1) == m);
	CHECK(chacha20_dec(key, c2) == m);
}

TEST_CASE("xchacha20_128_highlevel-basic-sanity", "[xchacha20][API]") {
	const auto key = key_128{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
	const auto m = "Hello World!";
	const auto c1 = xchacha20_enc(key, m);
	CHECK(m != c1);
	const auto c2 = xchacha20_enc(key, m);
	CHECK(m != c2);
	// This may fail once during 2^64 tests, in other words never:
	CHECK(c1 != c2);
	CHECK(xchacha20_dec(key, c1) == m);
	CHECK(xchacha20_dec(key, c2) == m);
}

TEST_CASE("xchacha20_256_highlevel-basic-sanity", "[xchacha20][API]") {
	const auto key = key_256{
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
	const auto m = "Hello World!";
	const auto c1 = xchacha20_enc(key, m);
	CHECK(m != c1);
	const auto c2 = xchacha20_enc(key, m);
	CHECK(m != c2);
	// This may fail once during 2^64 tests, in other words never:
	CHECK(c1 != c2);
	CHECK(xchacha20_dec(key, c1) == m);
	CHECK(xchacha20_dec(key, c2) == m);
}


} // namespace test
} // namespace chacha20
} // namespace mcp

