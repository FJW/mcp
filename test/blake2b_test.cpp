
#include "catch.hpp"

#include <mcp/blake2b.hpp>

#include <iostream>
#include <iomanip>

namespace mcp {
namespace blake2b {
namespace test {
TEST_CASE("blake2b_256-empty", "[blake2b]") {
	const auto m = std::string{};
	const auto expected = blake2b_256_hash{from_hex(
		"0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8")};
	const auto actual = blake2b_256(m);
	CHECK(actual == expected);
}


TEST_CASE("blake2b_256-hello-world", "[blake2b]") {
	const auto m = "Hello World!\n";
	const auto expected = blake2b_256_hash{from_hex(
		"f497a36252fe0182836a19a75de5d75996a0f0a4e19f81fe749aa9e809a1150c")};
	const auto actual = blake2b_256(m);
	CHECK(actual == expected);
}


TEST_CASE("blake2b_384-empty", "[blake2b]") {
	const auto m = std::string{};
	const auto expected = blake2b_384_hash{from_hex(
		"b32811423377f52d7862286ee1a72ee540524380fda1724a6f25d7978c6fd324"
		"4a6caf0498812673c5e05ef583825100")};
	const auto actual = blake2b_384(m);
	CHECK(actual == expected);
}


TEST_CASE("blake2b_384-hello-world", "[blake2b]") {
	const auto m = "Hello World!\n";
	const auto expected = blake2b_384_hash{from_hex(
		"bf67768ea242f1363f2f15bfde49a75c2cb71b230c6fb29f9ee38f522a6f36b5"
		"577663b291bce2d6ca5c21a37b842fa0"
		)};
	const auto actual = blake2b_384(m);
	CHECK(actual == expected);
}

TEST_CASE("blake2b_512-empty", "[blake2b]") {
	const auto m = std::string{};
	const auto expected = blake2b_512_hash{from_hex(
		"786a02f742015903c6c6fd852552d272912f4740e15847618a86e217f71f5419"
		"d25e1031afee585313896444934eb04b903a685b1448b755d56f701afe9be2ce")};
	const auto actual = blake2b_512(m);
	CHECK(actual == expected);
}


TEST_CASE("blake2b_512-hello-world", "[blake2b]") {
	const auto m = "Hello World!\n";
	const auto expected = blake2b_512_hash{from_hex(
		"4bdd5725495be9111e8818bcc916c68658d42ddbb3d22743e88215a21d7a26c7"
		"7666c2e69e61e0ed5dba881aefc8eda5006cadd09cf773e41f60db6988f3b19c")};
	const auto actual = blake2b_512(m);
	CHECK(actual == expected);
}

} // namespace test
} // namespace blake2b;
} // namespace mcp

