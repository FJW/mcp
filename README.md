
Modern Crypto Primitives (MCP)
==============================

**THIS LIBRARY IS UNREVIEWED AND NOT HARDENED AT ALL! ASSUME IT TO BE HIGHLY INSECURE AND DO NOT USE IT FOR ANY PRACTICAL PURPOSE WHATSOEVER!!!**

This library is an experiment to see what a modern C++-crypto-library could look like. The target is to create strongly typed, idiomatic interfaces
that make most errors impossible to begin with.


On a higher level, three libraries would be desirable:
* Modern Crypto Sockets (MCS), a TLS-implmentation
* Modern Crypto Tools (MCT), a collection of medium-level utilites, like commitments, MACs and similar applications with replacable underlying primitives.
* Modern Crypto Primitives (MCP), a collection of well-known and modern primitives


Overview
--------

The desired primitives for MCP are:
* Hashes
	* SHA2
	* SHA3
	* Blake2
* Password-Hashes
	* Argon2
* Blockciphers
	* AES
	* Camellia
* Blockcipher-modes
	* CBC
	* CTR
	* GCM
* Streamciphers
	* Salsa20
	* Chacha20
* Signatures
	* RSA
	* SPHINCS (?)
* Key-Exchange-Protokolls
	* ECDH
	* Latice-based (?)
* PK-ciphers
	* RSA
	* ECDH-based
	* Latice-based (?)
* Utilities
	* Cryptographically Secure RNG


Rationale
---------

Some of the above decisions demand some explanation:
* Camellia is a Feistel-Cipher and the only blockcipher aside from AES that is
  recommended by [ENISA](https://www.enisa.europa.eu/publications/algorithms-key-size-and-parameters-report-2014),
  Europes NIST-equivalent.
* [Argon2](https://en.wikipedia.org/wiki/Argon2) is the winner of the [Password-Hashing-competition](https://password-hashing.net/).
  Since the demands on password-hashes are very different from those on other hashes (being able to find
  collisions doesn't really matter that much, but being able to calculate them efficiently is not just not
  a feature but an attack!) Which is why putting some algorithms that are actually designed for it on this
  list.
* [SPHINCS](https://sphincs.cr.yp.to/) is a post-quantum signature-algorithm whose security only depends on
  the availability of hash-functions. While still quite new, breaking it would also break other well known
  algorithms, which combined with the pq-property is enough for me to put it on the wishlist.
* (EC-)DSA is missing from the list because using a nounce twice results in trivial key-extraction.
  While it is acceptable to see some breakage in that case, given the never-ending story of bad RNGs,
  I dislike the property enough to drop it from the wishlist.
* RSA is on the list because it is widely known and still secure given sufficient key-sizes. The
  signature-variant has the great advantage that it is deterministic and once you have that, there
  is little point not to include the as well known PK-encryption-scheme.
* Latice-based algorithms are one hope for post-quantum-cryptography. While it is not advisable to
  use them now, including them in an experimental-namespace could be interessting and combining them
  with well-known algorithms could ensure at least that security-level.


Status
------

Nothing has received any review as of now, but Salsa20 and SHA2 have unittests that suggest, that their results are correct.
(Nothing is known about side-channels, but both algorithms seem to be designed to avoid most common attacks of that kind.)

Currently implemented:
* Salsa20
* SHA2
* CSRNG

