
#include "mcp/chacha20.hpp"

#include <algorithm>
#include <cstring>
#include <functional>

#include "chacha20_detail.hpp"

namespace mcp {
namespace chacha20 {

namespace detail {


void quarterround(word& a, word& b, word& c, word& d) noexcept {
	a += b;
	d = lrot<16>(d xor a);
	c += d;
	b = lrot<12>(b xor c);
	a += b;
	d = lrot<8>(d xor a);
	c += d;
	b = lrot<7>(b xor c);
}


void doubleround(word_block_16& y) noexcept {
	quarterround(y[ 0], y[ 4], y[ 8], y[12]);
	quarterround(y[ 1], y[ 5], y[ 9], y[13]);
	quarterround(y[ 2], y[ 6], y[10], y[14]);
	quarterround(y[ 3], y[ 7], y[11], y[15]);

	quarterround(y[ 0], y[ 5], y[10], y[15]);
	quarterround(y[ 1], y[ 6], y[11], y[12]);
	quarterround(y[ 2], y[ 7], y[ 8], y[13]);
	quarterround(y[ 3], y[ 4], y[ 9], y[14]);
}


word_block_16 chacha20_hash_words(const word_block_16& x) noexcept {
	auto y = x;
	for (auto i = 0u; i < 10u; ++i) {
		doubleround(y);
	}
	std::transform(x.begin(), x.end(), y.begin(), y.begin(), std::plus<word>{});
	return y;
}


byte_block_64 chacha20_hash(byte_block_64 x) noexcept {
	auto tmp = word_block_16{};
	for (auto i = 0u; i < x.size(); i +=4) {
		tmp[i/4] = read_le_word(x.data() + i);
	}
	tmp = chacha20_hash_words(tmp);
	for (auto i = 0u; i < x.size(); i +=4) {
		const auto w = word{tmp[i/4]};
		x[i  ] = inv_little_endian<0>(w);
		x[i+1] = inv_little_endian<1>(w);
		x[i+2] = inv_little_endian<2>(w);
		x[i+3] = inv_little_endian<3>(w);
	}
	return x;
}


byte_block_64 chacha20_expand(const byte_block_32& k, const byte_block_16& n) noexcept {
	static const byte* constant = reinterpret_cast<const byte*>("expand 32-byte k");
	auto input_block = byte_block_64{};
	std::copy_n(constant,  16, input_block.begin());
	std::copy_n(k.begin(), 32, input_block.begin() + 16);
	std::copy_n(n.begin(), 16, input_block.begin() + 48);
	return chacha20_hash(input_block);
}


byte_block_64 chacha20_expand(const byte_block_16& k, const byte_block_16& n) noexcept {
	static const byte* constant = reinterpret_cast<const byte*>("expand 16-byte k");
	auto input_block = byte_block_64{};
	std::copy_n(constant,  16, input_block.begin());
	std::copy_n(k.begin(), 16, input_block.begin() + 16);
	std::copy_n(k.begin(), 16, input_block.begin() + 32);
	std::copy_n(n.begin(), 16, input_block.begin() + 48);
	return chacha20_hash(input_block);
}


key_256 make_xchacha_key(const key_128& k, const nounce_128& n) {
	return {chacha20_expand(k, n).data()};
}


key_256 make_xchacha_key(const key_256& k, const nounce_128& n) {
	return {chacha20_expand(k, n).data()};
}


std::pair<key_256, nounce_64> make_xchacha_kn_pair(const key_128& k, const nounce_192& n) {
	const auto big_nounce = nounce_128{n.data()};
	const auto small_nounce = nounce_64{n.data() + 128};
	return {make_xchacha_key(k, big_nounce), small_nounce};
}


std::pair<key_256, nounce_64> make_xchacha_kn_pair(const key_256& k, const nounce_192& n) {
	const auto big_nounce = nounce_128{n.data()};
	const auto small_nounce = nounce_64{n.data() + 128};
	return {make_xchacha_key(k, big_nounce), small_nounce};
}


} // namespace detail
} // namespace chacha20
} // namespace mcp

