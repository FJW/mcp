
#include "mcp/rng.hpp"

#include <algorithm>
#include <cassert>
#include <cstring>
#include <random>

#ifndef MCP_RANDOM_SOURCE_PATH
// fall back to default-ctor if nothing was provided
#define MCP_RANDOM_SOURCE_PATH
#endif


namespace mcp {

namespace {


std::random_device& get_rng() {
	thread_local std::random_device rd{MCP_RANDOM_SOURCE_PATH};
	return rd;
}


static_assert(64u % (sizeof(unsigned) * 8) == 0u, "unsigned must have at most blocksize");


void random_fill_block(byte* out, std::size_t n) {
	assert(n % sizeof(unsigned) == 0u);
	while (n > sizeof(unsigned)) {
		const auto entropy = get_rng()();
		std::memcpy(out, &entropy, sizeof(unsigned));
		out += sizeof(unsigned);
		n -= sizeof(unsigned);
	}
}


} // anonymous namespace


void random_fill(byte* out, std::size_t n) {
	while (n > sizeof(unsigned)) {
		const auto entropy = unsigned{get_rng()()};
		std::memcpy(out, &entropy, sizeof(unsigned));
		out += sizeof(unsigned);
	}
	if (n == 0u) {
		return;
	}
	const auto entropy = get_rng()();
	auto in_ptr = reinterpret_cast<const byte*>(&entropy);
	for (auto i = 0u; i < n; ++i) {
		*out++ = *in_ptr++;
	}
}


key_128 gen_key_128() {
	auto key = key_128{};
	random_fill_block(key.data(), key.size());
	return key;
}


key_192 gen_key_192() {
	auto key = key_192{};
	random_fill_block(key.data(), key.size());
	return key;
}


key_256 gen_key_256() {
	auto key = key_256{};
	random_fill_block(key.data(), key.size());
	return key;
}


nounce_64 gen_nounce_64() {
	auto nounce = nounce_64{};
	random_fill_block(nounce.data(), nounce.size());
	return nounce;
}


nounce_128 gen_nounce_128() {
	auto nounce = nounce_128{};
	random_fill_block(nounce.data(), nounce.size());
	return nounce;
}


nounce_192 gen_nounce_192() {
	auto nounce = nounce_192{};
	random_fill_block(nounce.data(), nounce.size());
	return nounce;
}


nounce_256 gen_nounce_256() {
	auto nounce = nounce_256{};
	random_fill_block(nounce.data(), nounce.size());
	return nounce;
}

} // namespace mcp


