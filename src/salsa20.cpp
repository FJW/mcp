
#include "mcp/salsa20.hpp"
#include "mcp/errors.hpp"
#include "mcp/rng.hpp"

#include <algorithm>
#include <cstring>
#include <functional>

#include "salsa20_detail.hpp"

namespace mcp {
namespace salsa20 {

using detail::blocksize;

namespace lowlevel {
void salsa20_stream(const key_128& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept {
	return detail::salsa20_stream(key, nounce, out, n);
}


void salsa20_stream(const key_256& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept {
	return detail::salsa20_stream(key, nounce, out, n);
}


void salsa20(const key_128& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept {
	return detail::salsa20(key, nounce, in, n, out);
}


void salsa20(const key_256& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept {
	return detail::salsa20(key, nounce, in, n, out);
}
} // namespace lowlevel



inline namespace highlevel {
std::string salsa20(const key_128& key, const nounce_64& nounce, const std::string& m) {
	auto c = std::string(m.size(), '\0');
	lowlevel::salsa20(key, nounce, reinterpret_cast<const byte*>(m.c_str()), m.size(), reinterpret_cast<byte*>(&c.front()));
	return c;
}


std::string salsa20(const key_256& key, const nounce_64& nounce, const std::string& m) {
	auto c = std::string(m.size(), '\0');
	lowlevel::salsa20(key, nounce, reinterpret_cast<const byte*>(m.c_str()), m.size(), reinterpret_cast<byte*>(&c.front()));
	return c;
}


std::string salsa20_enc(const key_128& key, const std::string& m) {
	auto nounce = gen_nounce_64();
	return std::string(nounce.begin(), nounce.end()) + salsa20(key, nounce, m);
}


std::string salsa20_enc(const key_256& key, const std::string& m) {
	auto nounce = gen_nounce_64();
	return std::string(nounce.begin(), nounce.end()) + salsa20(key, nounce, m);
}


std::string salsa20_dec(const key_128& key, const std::string& m) {
	if (m.size() < 8u) {
		throw input_length_error{"salsa20_dec: string shorter than nounce"};
	}
	const auto nounce = nounce_64{reinterpret_cast<const byte*>(m.data())};
	auto ret = std::string(m.size() - nounce.size(), '\0');
	lowlevel::salsa20(key, nounce, reinterpret_cast<const byte*>(m.data()) + nounce.size(), ret.size(),
			reinterpret_cast<byte*>(&ret.front()));
	return ret;
}


std::string salsa20_dec(const key_256& key, const std::string& m) {
	if (m.size() < 8u) {
		throw input_length_error{"salsa20_dec: string shorter than nounce"};
	}
	const auto nounce = nounce_64{reinterpret_cast<const byte*>(m.data())};
	auto ret = std::string(m.size() - nounce.size(), '\0');
	lowlevel::salsa20(key, nounce, reinterpret_cast<const byte*>(m.data()) + nounce.size(), ret.size(),
			reinterpret_cast<byte*>(&ret.front()));
	return ret;
}
} // namespace highlevel


} // namespace salsa20
} // namespace mcp

