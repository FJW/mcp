
#include "mcp/chacha20.hpp"

#include <algorithm>
#include <cstring>
#include <functional>

#include "mcp/rng.hpp"

#include "chacha20_detail.hpp"

namespace mcp {
namespace chacha20 {

using detail::blocksize;

namespace lowlevel {
void chacha20_stream(const key_128& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept {
	return detail::chacha20_stream(key, nounce, out, n);
}


void chacha20_stream(const key_256& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept {
	return detail::chacha20_stream(key, nounce, out, n);
}


void chacha20(const key_128& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept {
	return detail::chacha20(key, nounce, in, n, out);
}


void chacha20(const key_256& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept {
	return detail::chacha20(key, nounce, in, n, out);
}
} // namespace lowlevel



inline namespace highlevel {
std::string chacha20(const key_128& key, const nounce_64& nounce, const std::string& m) {
	auto c = std::string(m.size(), '\0');
	lowlevel::chacha20(key, nounce, reinterpret_cast<const byte*>(m.c_str()), m.size(), reinterpret_cast<byte*>(&c.front()));
	return c;
}


std::string chacha20(const key_256& key, const nounce_64& nounce, const std::string& m) {
	auto c = std::string(m.size(), '\0');
	lowlevel::chacha20(key, nounce, reinterpret_cast<const byte*>(m.c_str()), m.size(), reinterpret_cast<byte*>(&c.front()));
	return c;
}


std::string chacha20_enc(const key_128& key, const std::string& m) {
	auto nounce = gen_nounce_64();
	return std::string(nounce.begin(), nounce.end()) + chacha20(key, nounce, m);
}


std::string chacha20_enc(const key_256& key, const std::string& m) {
	auto nounce = gen_nounce_64();
	return std::string(nounce.begin(), nounce.end()) + chacha20(key, nounce, m);
}


std::string chacha20_dec(const key_128& key, const std::string& m) {
	if (m.size() < 8u) {
		throw input_length_error{"chacha20_dec: string shorter than nounce"};
	}
	const auto nounce = nounce_64{reinterpret_cast<const byte*>(m.data())};
	auto ret = std::string(m.size() - nounce.size(), '\0');
	lowlevel::chacha20(key, nounce, reinterpret_cast<const byte*>(m.data()) + nounce.size(), ret.size(),
			reinterpret_cast<byte*>(&ret.front()));
	return ret;
}


std::string chacha20_dec(const key_256& key, const std::string& m) {
	if (m.size() < 8u) {
		throw input_length_error{"chacha20_dec: string shorter than nounce"};
	}
	const auto nounce = nounce_64{reinterpret_cast<const byte*>(m.data())};
	auto ret = std::string(m.size() - nounce.size(), '\0');
	lowlevel::chacha20(key, nounce, reinterpret_cast<const byte*>(m.data()) + nounce.size(), ret.size(),
			reinterpret_cast<byte*>(&ret.front()));
	return ret;
}
} // namespace highlevel

/// xchacha

namespace lowlevel {
void xchacha20_stream(const key_128& key, const nounce_192& nounce, byte* out, std::size_t n) noexcept {
	const auto kn_pair = detail::make_xchacha_kn_pair(key, nounce);
	chacha20_stream(kn_pair.first, kn_pair.second, out, n);
}


void xchacha20_stream(const key_256& key, const nounce_192& nounce, byte* out, std::size_t n) noexcept {
	const auto kn_pair = detail::make_xchacha_kn_pair(key, nounce);
	chacha20_stream(kn_pair.first, kn_pair.second, out, n);
}


void xchacha20(const key_128& key, const nounce_192& nounce, const byte* in, std::size_t n, byte* out) noexcept {
	const auto kn_pair = detail::make_xchacha_kn_pair(key, nounce);
	chacha20(kn_pair.first, kn_pair.second, in, n, out);
}


void xchacha20(const key_256& key, const nounce_192& nounce, const byte* in, std::size_t n, byte* out) noexcept {
	const auto kn_pair = detail::make_xchacha_kn_pair(key, nounce);
	chacha20(kn_pair.first, kn_pair.second, in, n, out);
}
} // namespace lowlevel

inline namespace highlevel {
std::string xchacha20(const key_128& key, const nounce_192& nounce, const std::string& m) {
	const auto kn_pair = detail::make_xchacha_kn_pair(key, nounce);
	return chacha20(kn_pair.first, kn_pair.second, m);
}


std::string xchacha20(const key_256& key, const nounce_192& nounce, const std::string& m) {
	const auto kn_pair = detail::make_xchacha_kn_pair(key, nounce);
	return chacha20(kn_pair.first, kn_pair.second, m);
}


std::string xchacha20_enc(const key_128& key, const std::string& m) {
	auto big_nounce = gen_nounce_128();
	return std::string(big_nounce.begin(), big_nounce.end()) + chacha20_enc(detail::make_xchacha_key(key, big_nounce), m);
}


std::string xchacha20_enc(const key_256& key, const std::string& m) {
	auto big_nounce = gen_nounce_128();
	return std::string(big_nounce.begin(), big_nounce.end()) + chacha20_enc(detail::make_xchacha_key(key, big_nounce), m);
}


std::string xchacha20_dec(const key_128& key, const std::string& m) {
	constexpr const auto nounce_size = 24u;
	if (m.size() < nounce_size) {
		throw input_length_error{"xchacha20_dec: string shorter than nounce"};
	}
	const auto b_nounce = nounce_128{reinterpret_cast<const byte*>(m.data())};
	const auto s_nounce = nounce_64{reinterpret_cast<const byte*>(m.data()) + 16};
	auto ret = std::string(m.size() - nounce_size, '\0');
	lowlevel::chacha20(detail::make_xchacha_key(key, b_nounce), s_nounce, reinterpret_cast<const byte*>(m.data()) + nounce_size, ret.size(),
			reinterpret_cast<byte*>(&ret.front()));
	return ret;
}


std::string xchacha20_dec(const key_256& key, const std::string& m) {
	constexpr const auto nounce_size = 24u;
	if (m.size() < nounce_size) {
		throw input_length_error{"xchacha20_dec: string shorter than nounce"};
	}
	const auto b_nounce = nounce_128{reinterpret_cast<const byte*>(m.data())};
	const auto s_nounce = nounce_64{reinterpret_cast<const byte*>(m.data()) + 16};
	auto ret = std::string(m.size() - nounce_size, '\0');
	lowlevel::chacha20(detail::make_xchacha_key(key, b_nounce), s_nounce, reinterpret_cast<const byte*>(m.data()) + nounce_size, ret.size(),
			reinterpret_cast<byte*>(&ret.front()));
	return ret;
}


} // namespace highlevel

} // namespace chacha20
} // namespace mcp

