
#include <mcp/core.hpp>

#include "utils.hpp"

namespace mcp {

using detail::hex_to_byte;


void read_hex(byte* out, std::size_t n, const char* str) {
	for (auto i = std::size_t{}; i < n; ++i) {
		*out++ = static_cast<byte>(hex_to_byte(str[2u*i])*16 + hex_to_byte(str[2u*i + 1]));
	}
}


} // namespace mcp




