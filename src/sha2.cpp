
#include "sha2_256_detail.hpp"
#include "sha2_512_detail.hpp"

#include <algorithm>

#include "utils.hpp"

namespace mcp {
namespace sha2 {

namespace lowlevel {
sha2_256_hash sha2_256(const byte* ptr, std::size_t length) {
	const auto result = detail::sha2_256(ptr, length);
	auto retval = sha2_256_hash{};
	auto ret_ptr = retval.data();
	for (auto w: result) {
		//mcp::detail::word_to_bytes(ret_ptr, w);
		mcp::detail::write_be(ret_ptr, w);
		ret_ptr += 4;
	}
	return retval;
}


sha2_256_hash sha2_256(const char* ptr, std::size_t length) {
	return sha2_256(reinterpret_cast<const byte*>(ptr), length);
}


sha2_384_hash sha2_384(const byte* ptr, std::size_t length) {
	const auto result = detail::sha2_384(ptr, length);
	auto retval = sha2_384_hash{};
	auto ret_ptr = retval.data();
	for (auto i = 0u; i < 6; ++i) {
		mcp::detail::write_be(ret_ptr, result[i]);
		ret_ptr += 8;
	}
	return retval;
}


sha2_384_hash sha2_384(const char* ptr, std::size_t length) {
	return sha2_384(reinterpret_cast<const byte*>(ptr), length);
}


sha2_512_hash sha2_512(const byte* ptr, std::size_t length) {
	const auto result = detail::sha2_512(ptr, length);
	auto retval = sha2_512_hash{};
	auto ret_ptr = retval.data();
	for (auto w: result) {
		mcp::detail::write_be(ret_ptr, w);
		ret_ptr += 8;
	}
	return retval;
}


sha2_512_hash sha2_512(const char* ptr, std::size_t length) {
	return sha2_512(reinterpret_cast<const byte*>(ptr), length);
}
}// namespace lowlevel

inline namespace highlevel {
sha2_256_hash sha2_256(const std::string& m) {
	return lowlevel::sha2_256(m.c_str(), m.size());
}


sha2_384_hash sha2_384(const std::string& m) {
	return lowlevel::sha2_384(m.c_str(), m.size());
}


sha2_512_hash sha2_512(const std::string& m) {
	return lowlevel::sha2_512(m.c_str(), m.size());
}
} // namespace highlevel

} // namespace sha2
} // namespace mcp
