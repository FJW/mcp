

#include <mcp/blake2b.hpp>

#include <algorithm>

#include "utils.hpp"
#include "dword.hpp"

namespace mcp {
namespace blake2b {
namespace detail {

using namespace mcp::detail;

constexpr auto indices = std::array<std::uint8_t, 12u * 16u>{{
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
	14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3,
	11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4,
	7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8,
	9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13,
	2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9,
	12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11,
	13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10,
	6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5,
	10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0,
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
	14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3}};

constexpr auto iv = dword_block_8 {{
	0x6a09e667f3bcc908,
	0xbb67ae8584caa73b,
	0x3c6ef372fe94f82b,
	0xa54ff53a5f1d36f1,
	0x510e527fade682d1,
	0x9b05688c2b3e6c1f,
	0x1f83d9abfb41bd6b,
	0x5be0cd19137e2179
}};

template<unsigned Round, unsigned I>
void g_fun(dword& a, dword& b, dword& c, dword& d, const dword_block_16& m) {
	a += b + m[indices[Round * 16 + 2 * I]];
	d = rrot<32>(d xor a);
	c += d;
	b = rrot<24>(b xor c);
	a += b + m[indices[Round * 16 + 2 * I + 1]];
	d = rrot<16>(d xor a);
	c += d;
	b = rrot<63>(b xor c);
}


template<unsigned Round>
void round(dword_block_16& v, const dword_block_16& m) {
	g_fun<Round, 0>(v[0],v[4],v[8],v[12], m);
	g_fun<Round, 1>(v[1],v[5],v[9],v[13], m);
	g_fun<Round, 2>(v[2],v[6],v[10],v[14], m);
	g_fun<Round, 3>(v[3],v[7],v[11],v[15], m);
	g_fun<Round, 4>(v[0],v[5],v[10],v[15], m);
	g_fun<Round, 5>(v[1],v[6],v[11],v[12], m);
	g_fun<Round, 6>(v[2],v[7],v[8],v[13], m);
	g_fun<Round, 7>(v[3],v[4],v[9],v[14], m);
}

inline void finalize_compress(dword_block_8& h, const dword_block_16& v) {
	h[0] ^= v[0] xor v[8];
	h[1] ^= v[1] xor v[9];
	h[2] ^= v[2] xor v[10];
	h[3] ^= v[3] xor v[11];
	h[4] ^= v[4] xor v[12];
	h[5] ^= v[5] xor v[13];
	h[6] ^= v[6] xor v[14];
	h[7] ^= v[7] xor v[15];
}

inline void compress(dword_block_8& h, const dword_block_16& m, const dword_block_4& l) {
	auto v = dword_block_16{};
	std::copy(h.begin(), h.end(), v.begin());
	std::copy(iv.begin(), iv.end(), v.begin() + 8);
	std::transform(l.begin(), l.end(), v.begin() + 12, v.begin() + 12, std::bit_xor<dword>{});
	round< 0>(v, m);
	round< 1>(v, m);
	round< 2>(v, m);
	round< 3>(v, m);
	round< 4>(v, m);
	round< 5>(v, m);
	round< 6>(v, m);
	round< 7>(v, m);
	round< 8>(v, m);
	round< 9>(v, m);
	round<10>(v, m);
	round<11>(v, m);
	finalize_compress(h, v);
}

using u8 = std::uint8_t;

void fill_parameter_block(dword_block_8& block, u8 digest_length, u8 key_length = 0u) {
	auto tmp = byte_block_64{};
	std::fill(tmp.begin(), tmp.end(), 0);
	tmp[0] = digest_length;
	tmp[1] = key_length; // key-length
	tmp[2] = 1; // fanout
	tmp[3] = 1; // depth
	// tmp[4..7] = 0; // leaf-length
	// tmp[8..15] = 0; // node_offset
	// tmp[16] = 0; // node-depth
	// tmp[17] = 0; // inner-length
	// tmp[18..31] = 0; // reserved
	// tmp[32..47] = 0; // salt
	// tmp[48..63] = 0; // personalization
	std::copy_n(tmp.data(), 64, reinterpret_cast<byte*>(block.data()));
}

void compute_digest(byte* out, u8 digest_length, const byte* it, const byte* end) {
	auto h = dword_block_8{};
	fill_parameter_block(h, digest_length);
	xor_assign(h, iv);
	auto l = dword_block_4{{0,0,0,0}};
	auto m = dword_block_16{};
	while (std::distance(it, end) > 128) {
		std::copy_n(it, 128, reinterpret_cast<byte*>(m.data()));
		compress(h, m, l);
		it += 128;
		l[0] += 128;
	}
	const auto remaining = std::distance(it, end);
	std::copy_n(it, remaining, reinterpret_cast<byte*>(m.data()));
	std::fill_n(reinterpret_cast<byte*>(m.data()) + remaining, 128 - remaining, 0);
	l[0] += static_cast<dword>(remaining); // update counter
	l[2] = dword_max; // f0 set to last block
	//l[3] = dword_max; // for some reason f1 must not be set to last block
	compress(h, m, l);
	std::copy_n(reinterpret_cast<const byte*>(h.data()), digest_length, out);
}

} // namespace detail

namespace lowlevel {

blake2b_256_hash blake2b_256(const byte* ptr, std::size_t length) {
	auto ret = blake2b_256_hash{};
	detail::compute_digest(ret.data(), 256/8, ptr, ptr + length);
	return ret;
}
blake2b_256_hash blake2b_256(const char* cptr, std::size_t length){
	const auto ptr = reinterpret_cast<const byte*>(cptr);
	auto ret = blake2b_256_hash{};
	detail::compute_digest(ret.data(), 256/8, ptr, ptr + length);
	return ret;
}
blake2b_384_hash blake2b_384(const byte* ptr, std::size_t length){
	auto ret = blake2b_384_hash{};
	detail::compute_digest(ret.data(), 384/8, ptr, ptr + length);
	return ret;
}
blake2b_384_hash blake2b_384(const char* cptr, std::size_t length){
	const auto ptr = reinterpret_cast<const byte*>(cptr);
	auto ret = blake2b_384_hash{};
	detail::compute_digest(ret.data(), 384/8, ptr, ptr + length);
	return ret;
}
blake2b_512_hash blake2b_512(const byte* ptr, std::size_t length){
	auto ret = blake2b_512_hash{};
	detail::compute_digest(ret.data(), 512/8, ptr, ptr + length);
	return ret;
}
blake2b_512_hash blake2b_512(const char* cptr, std::size_t length){
	const auto ptr = reinterpret_cast<const byte*>(cptr);
	auto ret = blake2b_512_hash{};
	detail::compute_digest(ret.data(), 512/8, ptr, ptr + length);
	return ret;
}
}

inline namespace highlevel {
blake2b_256_hash blake2b_256(const std::string& m) {
	return lowlevel::blake2b_256(m.data(), m.size());
}
blake2b_384_hash blake2b_384(const std::string& m) {
	return lowlevel::blake2b_384(m.data(), m.size());
}
blake2b_512_hash blake2b_512(const std::string& m) {
	return lowlevel::blake2b_512(m.data(), m.size());
}
} // namespace highlevel

} // namespace blake2b
} // namespace mcp
