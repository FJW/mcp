
#include "sha2_512_detail.hpp"

#include <algorithm>
#include <functional>


namespace mcp {
namespace sha2 {
namespace detail {

using mcp::detail::rrot;
using mcp::detail::rshift;
using mcp::detail::read_be_dword;
using mcp::detail::read_be_dword_n;

constexpr auto state_size_bit = 512u;
constexpr auto state_size_byte = state_size_bit / 8u;
constexpr auto m_blocksize_bit = 1024u;
constexpr auto m_blocksize_byte = m_blocksize_bit / 8u;

dword_block_8 sha2_512(const byte* m, std::size_t l) {
	auto state = init_512;
	auto buffer = dword_block_16{};
	while (l > m_blocksize_byte) {
		read_block(buffer, m);
		attach_block(state, buffer);
		m += m_blocksize_byte;
		l -= m_blocksize_byte;
	}
	if (l + 1u + 32u <= m_blocksize_byte) {
		add_padding_1(buffer, m, l);
		attach_block(state, buffer);
	} else {
		const auto buffer2 = add_padding_2(buffer, m, l);
		attach_block(state, buffer);
		attach_block(state, buffer2);
	}
	return state;
}


dword_block_8 sha2_384(const byte* m, std::size_t l) {
	auto state = init_384;
	auto buffer = dword_block_16{};
	while (l > m_blocksize_byte) {
		read_block(buffer, m);
		attach_block(state, buffer);
		m += m_blocksize_byte;
		l -= m_blocksize_byte;
	}
	if (l + 1u + 16u <= 64u) {
		add_padding_1(buffer, m, l);
		attach_block(state, buffer);
	} else {
		const auto buffer2 = add_padding_2(buffer, m, l);
		attach_block(state, buffer);
		attach_block(state, buffer2);
	}
	return state;
}


void read_block(dword_block_16& block, const byte* m) {
	for (auto i = 0u; i < 16u; ++i, m+=4) {
		block[i] = read_be_dword(m);
	}
}


dword shiffted_end_byte_dword(unsigned n) {
	return dword{dword{1u} << ((sizeof(dword) - n -1u) * 8u + 7u)};
}

void add_padding_1(dword_block_16& buf, const byte* m, const std::size_t l) {
	std::fill(buf.begin(), buf.end(), 0u);
	auto i = 0u;
	for (auto r = l%m_blocksize_byte; r >= 8u; r -= 8, m+=8) {
		buf[i++] = read_be_dword(m);
	}
	buf[i] = read_be_dword_n(m, l % 8u) bitor shiffted_end_byte_dword(l % 8u);
	buf[15] = dword{l * 8u};
}


dword_block_16 add_padding_2(dword_block_16& buf1, const byte* m, std::size_t l) {
	auto buf2 = dword_block_16{};
	std::fill(buf1.begin(), buf1.end(), 0u);
	std::fill(buf2.begin(), buf2.end(), 0u);
	auto i = 0u;
	for (auto r = l%m_blocksize_byte; r >= 8u; r -= 8, m+=8) {
		buf1[i++] = read_be_dword(m);
	}
	// TODO: same as for 256-bit:
	if (l%m_blocksize_byte != sizeof(byte) * buf1.size()) {
		// there is enough space in the first block
		buf1[i] = read_be_dword_n(m, l % 8u) bitor shiffted_end_byte_dword(l % 8u);
	} else {
		// we only ever write one byte in the other case:
		buf2[0] = shiffted_end_byte_dword(0);
	}

	buf2[15] = dword{l * 8u};

	return buf2;
}


void attach_block(dword_block_8& state, dword_block_16 w) {
	auto local_state = state;
	for (auto i = 0u; i < 16; ++i) {
		compression_512(local_state, k_512[i], w[i]);
	}
	for (auto i = 16u; i < 80; ++i) {
		compression_512(local_state, k_512[i], next_w(w));
	}
	std::transform(state.begin(), state.end(), local_state.begin(), state.begin(), std::plus<dword>{});
}


void compression_512(dword_block_8& state, dword k, dword w) {
	const auto t1 = dword{state[7] + sigma_1(state[4]) + ch(state[4], state[5], state[6]) + k + w};
	const auto t2 = dword{sigma_0(state[0]) + maj(state[0], state[1], state[2])};
	state[7] = state[6];
	state[6] = state[5];
	state[5] = state[4];
	state[4] = state[3] + t1;
	state[3] = state[2];
	state[2] = state[1];
	state[1] = state[0];
	state[0] = t1 + t2;
}

dword next_w(dword_block_16& w) {
	const auto res = small_sigma_1(w[16-2]) + w[16-7] + small_sigma_0(w[16-15]) + w[16-16];
	std::move(std::next(w.begin()), w.end(), w.begin());
	w.back() = res;
	return res;
}


dword ch(dword x, dword y, dword z) {
	return (x bitand y) xor ((~x) bitand z);
}


dword maj(dword x, dword y, dword z) {
	return (x bitand y) xor (x bitand z) xor (y bitand z);
}


dword sigma_0(dword x) {
	return rrot<28>(x) xor rrot<34>(x) xor rrot<39>(x);
}


dword sigma_1(dword x) {
	return rrot<14>(x) xor rrot<18>(x) xor rrot<41>(x);
}


dword small_sigma_0(dword x) {
	return rrot<1>(x) xor rrot<8>(x) xor rshift<7>(x);
}


dword small_sigma_1(dword x) {
	return rrot<19>(x) xor rrot<61>(x) xor rshift<6>(x);
}

} // namespace detail
} // namespace sha2
} // namespace mcp
