
#include "utils.hpp"

#include <cassert>


namespace mcp {
namespace detail {

word read_le_word(const byte* ptr) {
	return word{
		  word{ptr[0]} <<  0u
		| word{ptr[1]} <<  8u
		| word{ptr[2]} << 16u
		| word{ptr[3]} << 24u
	};
}

word read_be_word(const byte* ptr) {
	return word{
		  word{ptr[0]} << 24u
		| word{ptr[1]} << 16u
		| word{ptr[2]} <<  8u
		| word{ptr[3]} <<  0u
	};
}

void write_le(byte* ptr, word w) {
	ptr[0] = static_cast<byte>(w >>  0u);
	ptr[1] = static_cast<byte>(w >>  8u);
	ptr[2] = static_cast<byte>(w >> 16u);
	ptr[3] = static_cast<byte>(w >> 24u);
}

void write_be(byte* ptr, word w) {
	ptr[3] = static_cast<byte>(w >>  0u);
	ptr[2] = static_cast<byte>(w >>  8u);
	ptr[1] = static_cast<byte>(w >> 16u);
	ptr[0] = static_cast<byte>(w >> 24u);
}

word read_be_word_n(const byte* ptr, unsigned n) {
	auto ret = word{};
	for (auto i =0u; i < n; ++i) {
		ret |= word{ptr[i]};
		ret <<= 8u;
	}
	ret <<= 8u * (sizeof(word) - (n+1u));
	return ret;
}



} // namespace detail
} // namespace mcp


