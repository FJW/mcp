
#include "sha2_256_detail.hpp"

#include <algorithm>
#include <functional>


namespace mcp {
namespace sha2 {
namespace detail {

using mcp::detail::rrot;
using mcp::detail::rshift;
using mcp::detail::read_be_word;
using mcp::detail::read_be_word_n;


word_block_8 sha2_256(const byte* m, std::size_t l) {
	auto state = init_256;
	auto buffer = word_block_16{};
	while (l > 64u) {
		read_block(buffer, m);
		attach_block(state, buffer);
		m += 64u;
		l -= 64u;
	}
	if (l + 1u + 8u <= 64u) {
		add_padding_1(buffer, m, l);
		attach_block(state, buffer);
	} else {
		const auto buffer2 = add_padding_2(buffer, m, l);
		attach_block(state, buffer);
		attach_block(state, buffer2);
	}
	return state;
}


void read_block(word_block_16& block, const byte* m) {
	for (auto i = 0u; i < 16u; ++i, m+=4) {
		block[i] = read_be_word(m);
	}
}

word shiffted_end_byte(unsigned n) {
	return word{word{1u} << ((sizeof(word) - n -1u) * 8u + 7u)};
}

void add_padding_1(word_block_16& buf, const byte* m, const std::size_t l) {
	std::fill(buf.begin(), buf.end(), 0u);
	auto i = 0u;
	for (auto r = l%64u; r >= 4u; r -= 4, m+=4) {
		buf[i++] = read_be_word(m);
	}
	buf[i] = read_be_word_n(m, l % 4u) bitor shiffted_end_byte(l % 4u);
	const auto high_len = 8u * static_cast<word>(std::uint64_t{l} >> 32u);
	const auto low_len  = 8u * static_cast<word>(std::uint64_t{l});
	buf[14] = high_len;
	buf[15] = low_len;
}


word_block_16 add_padding_2(word_block_16& buf1, const byte* m, std::size_t l) {
	auto buf2 = word_block_16{};
	std::fill(buf1.begin(), buf1.end(), 0u);
	std::fill(buf2.begin(), buf2.end(), 0u);
	auto i = 0u;
	for (auto r = l%64u; r >= 4u; r -= 4, m+=4) {
		buf1[i++] = read_be_word(m);
	}
	if (l%64 != sizeof(byte) * buf1.size()) { // TODO!
		// there is enough space in the first block
		buf1[i] = read_be_word_n(m, l % 4u) bitor shiffted_end_byte(l % 4u);
	} else {
		// we only ever write one byte in the other case:
		buf2[0] = shiffted_end_byte(0); // TODO: unittest missing
	}

	const auto high_len = 8u * static_cast<word>(std::uint64_t{l} >> 32u);
	const auto low_len  = 8u * static_cast<word>(std::uint64_t{l});
	buf2[14] = high_len;
	buf2[15] = low_len;

	return buf2;
}


void attach_block(word_block_8& state, word_block_16 w) {
	auto local_state = state;
	for (auto i = 0u; i < 16; ++i) {
		compression_256(local_state, k_256[i], w[i]);
	}
	for (auto i = 16u; i < 64; ++i) {
		compression_256(local_state, k_256[i], next_w(w));
	}
	std::transform(state.begin(), state.end(), local_state.begin(), state.begin(), std::plus<word>{});
}


void compression_256(word_block_8& state, word k, word w) {
	const auto t1 = word{state[7] + sigma_1(state[4]) + ch(state[4], state[5], state[6]) + k + w};
	const auto t2 = word{sigma_0(state[0]) + maj(state[0], state[1], state[2])};
	state[7] = state[6];
	state[6] = state[5];
	state[5] = state[4];
	state[4] = state[3] + t1;
	state[3] = state[2];
	state[2] = state[1];
	state[1] = state[0];
	state[0] = t1 + t2;
}

word next_w(word_block_16& w) {
	const auto second = std::next(w.begin());
	const auto res = small_sigma_1(w[16-2]) + w[16-7] + small_sigma_0(w[16-15]) + w[16-16];
	std::move(second, w.end(), w.begin());
	w.back() = res;
	return res;
}


word ch(word x, word y, word z) {
	return (x bitand y) xor ((~x) bitand z);
}


word maj(word x, word y, word z) {
	return (x bitand y) xor (x bitand z) xor (y bitand z);
}


word sigma_0(word x) {
	return rrot<2>(x) xor rrot<13>(x) xor rrot<22>(x);
}


word sigma_1(word x) {
	return rrot<6>(x) xor rrot<11>(x) xor rrot<25>(x);
}


word small_sigma_0(word x) {
	return rrot<7>(x) xor rrot<18>(x) xor rshift<3>(x);
}


word small_sigma_1(word x) {
	return rrot<17>(x) xor rrot<19>(x) xor rshift<10>(x);
}

} // namespace detail
} // namespace sha2
} // namespace mcp
