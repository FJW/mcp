#pragma once

#include <cstdint>
#include <tuple>

#include "mcp/core.hpp"

#include "word.hpp"
#include "dword.hpp"

namespace mcp {
namespace detail {

byte hex_to_byte(char c);

template<unsigned Bytes>
void xor_with(byte_block<Bytes>& lhs, const byte_block<Bytes>& rhs) {
	for (auto i = byte{}; i < Bytes; ++i) {
		// lhs[i] ^= rhs[i];
		// cannot be used because of https://gcc.gnu.org/bugzilla/show_bug.cgi?id=38522
		// so we use this instead to shut GCC up:
		lhs[i] = lhs[i] xor rhs[i];
	}
}

template<unsigned Bytes>
byte_block<Bytes/2u> left_half(const byte_block<Bytes>& in) {
	return {from_raw(in.data())};
}
template<unsigned Bytes>
byte_block<Bytes/2u> right_half(const byte_block<Bytes>& in) {
	return {from_raw(in.data() + (Bytes / 2u))};
}

template<unsigned N>
byte_block_16 lrot(byte_block_16 x) {
	auto l = read_be_dword(x.data() + 0);
	auto r = read_be_dword(x.data() + 8);
	std::tie(l, r) = lrot<N>(l, r);
	write_be(x.data() + 0, l);
	write_be(x.data() + 8, r);
	return x;
}

} // namespace detail
} // namespace mcp


