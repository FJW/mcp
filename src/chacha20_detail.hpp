#pragma once

#include "mcp/chacha20.hpp"

#include <algorithm>
#include <cstring>
#include <functional>
#include <utility>

#include "utils.hpp"

namespace mcp {
namespace chacha20 {
namespace detail {

using namespace ::mcp::detail;

constexpr auto blocksize = 64u;

void quarterround(word& y0, word& y1, word& y2, word& y3) noexcept;
void doubleround(word_block_16& y) noexcept;

word_block_16 chacha20_hash_words(const word_block_16& x) noexcept;

byte_block_64 chacha20_hash(byte_block_64 x) noexcept;

byte_block_64 chacha20_expand(const byte_block_32& k, const byte_block_16& n) noexcept;
byte_block_64 chacha20_expand(const byte_block_16& k, const byte_block_16& n) noexcept;

key_256 make_xchacha_key(const key_128& k, const nounce_128& n);
key_256 make_xchacha_key(const key_256& k, const nounce_128& n);

std::pair<key_256, nounce_64> make_xchacha_kn_pair(const key_128& k, const nounce_192& n);
std::pair<key_256, nounce_64> make_xchacha_kn_pair(const key_256& k, const nounce_192& n);

template<typename Key>
void chacha20_stream(const Key& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept {
	auto ctr = byte_block_16{};
	std::copy(nounce.begin(), nounce.end(), ctr.begin());
	auto& i = *(reinterpret_cast<std::uint64_t*>(ctr.data() + 8));
	i = 0;
	while (n > blocksize) {
		const auto streamblock = detail::chacha20_expand(key, ctr);
		std::copy_n(streamblock.begin(), blocksize, out);
		++i;
		n -= blocksize;
		out += blocksize;
	}
	const auto streamblock = detail::chacha20_expand(key, ctr);
	std::copy_n(streamblock.begin(), n, out);
}

template<typename Key>
void chacha20(const Key& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept {
	auto ctr = byte_block_16{};
	std::copy(nounce.begin(), nounce.end(), ctr.begin());
	auto& i = *(reinterpret_cast<std::uint64_t*>(ctr.data() + 8));
	i = 0;
	while (n > blocksize) {
		const auto streamblock = detail::chacha20_expand(key, ctr);
		std::transform(in, in +n, streamblock.begin(), out, std::bit_xor<byte>{});
		++i;
		n -= blocksize;
		in += blocksize;
		out += blocksize;
	}
	const auto streamblock = detail::chacha20_expand(key, ctr);
	std::transform(in, in +n, streamblock.begin(), out, std::bit_xor<byte>{});
}


} // namespace detail
} // namespace chacha20
} // namespace mcp



