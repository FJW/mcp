
#include "utils.hpp"

namespace mcp {
namespace detail {


byte hex_to_byte(char c) {
	if ('0' <= c and c <= '9') {
		return static_cast<byte>(c-'0');
	} else if ('a' <= c and c <= 'f') {
		return static_cast<byte>((c-'a') + 10);
	} else if ('A' <= c and c <= 'F') {
		return static_cast<byte>((c - 'A') + 10);
	} else {
		throw mcp::parsing_error{"bad digit"};
	}
}


} // namespace detail
} // namespace mcp

