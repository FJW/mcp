#pragma once

#include <algorithm>
#include <numeric>
#include <cstdint>
#include <tuple>
#include <utility>
#include <type_traits>
#include <limits>

#include "mcp/core.hpp"

namespace mcp {
namespace detail {


using dword = std::uint64_t;
using dword_block_4 = std::array<dword, 4>;
using dword_block_8 = std::array<dword, 8>;
using dword_block_16 = std::array<dword, 16>;
using dword_block_128 = std::array<dword, 128>;

constexpr auto dword_max = std::numeric_limits<dword>::max();

template<std::size_t N>
void xor_assign(std::array<dword, N>& lhs, const std::array<dword, N>& rhs) {
	std::transform(lhs.begin(), lhs.end(), rhs.begin(), lhs.begin(), std::bit_xor<dword>{});
}

template<dword N>
dword rshift(dword x) {
	static_assert(N < 64, "");
	return x >> N;
}

template<dword N>
dword rrot(dword x) {
	static_assert(0 < N and N < 64, "");
	return (x >> N) bitor (x << (64 - N));
}

template<dword N>
dword lrot(dword x) {
	static_assert(0 < N and N < 64, "");
	return (x << N) bitor (x >> (64 - N));
}
template<>
inline dword lrot<0>(dword x) {
	return x;
}

dword read_le_dword(const byte* ptr);
dword read_be_dword(const byte* ptr);
void write_le(byte* ptr, dword w);
void write_be(byte* ptr, dword w);

dword read_be_dword_n(const byte* bytes, unsigned n);

template<dword N>
std::pair<dword, dword> lrot(dword l, dword r, std::true_type){
	constexpr auto k = N;
	static_assert(k < 64u,"");
	return {
		(l << k) bitor (r >> (64u - k)),
		(r << k) bitor (l >> (64u - k))
	};
}
template<dword N>
std::pair<dword, dword> lrot(dword l, dword r, std::false_type){
	constexpr auto k = 128u - N;
	static_assert(k < 64u,"");
	return {
		(l >> k) bitor (r << (64u - k)),
		(r >> k) bitor (l << (64u - k))
	};
}

template<dword N>
std::pair<dword, dword> lrot(dword l, dword r){
	return lrot<N>(l, r, std::integral_constant<bool, (N < 64)>{});
}
template<>
inline std::pair<dword, dword> lrot<0>(dword l, dword r) {
	return {l, r};
}
template<>
inline std::pair<dword, dword> lrot<64>(dword l, dword r) {
	return {r, l};
}

} // namespace detail
} // namespace mcp


