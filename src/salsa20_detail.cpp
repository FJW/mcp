
#include "mcp/salsa20.hpp"

#include <algorithm>
#include <cstring>
#include <functional>

#include "salsa20_detail.hpp"

namespace mcp {
namespace salsa20 {

namespace detail {


word_block_4 quarterround(word y0, word y1, word y2, word y3) noexcept {
	const auto z1 = word{y1 xor lrot< 7>(y0 + y3)};
	const auto z2 = word{y2 xor lrot< 9>(z1 + y0)};
	const auto z3 = word{y3 xor lrot<13>(z2 + z1)};
	const auto z0 = word{y0 xor lrot<18>(z3 + z2)};
	return word_block_4{z0, z1, z2, z3};
}


word_block_16 rowround(word_block_16 y) noexcept {
	std::tie(y[ 0], y[ 1], y[ 2], y[ 3]) = quarterround(y[ 0], y[ 1], y[ 2], y[ 3]);
	std::tie(y[ 5], y[ 6], y[ 7], y[ 4]) = quarterround(y[ 5], y[ 6], y[ 7], y[ 4]);
	std::tie(y[10], y[11], y[ 8], y[ 9]) = quarterround(y[10], y[11], y[ 8], y[ 9]);
	std::tie(y[15], y[12], y[13], y[14]) = quarterround(y[15], y[12], y[13], y[14]);
	return y;
}


word_block_16 columnround(word_block_16 y) noexcept {
	std::tie(y[ 0], y[ 4], y[ 8], y[12]) = quarterround(y[ 0], y[ 4], y[ 8], y[12]);
	std::tie(y[ 5], y[ 9], y[13], y[ 1]) = quarterround(y[ 5], y[ 9], y[13], y[ 1]);
	std::tie(y[10], y[14], y[ 2], y[ 6]) = quarterround(y[10], y[14], y[ 2], y[ 6]);
	std::tie(y[15], y[ 3], y[ 7], y[11]) = quarterround(y[15], y[ 3], y[ 7], y[11]);
	return y;
}


word_block_16 doubleround(word_block_16 y) noexcept {
	return rowround(columnround(y));
}

word_block_16 salsa20_hash_words(const word_block_16& x) noexcept {
	auto y = x;
	for (auto i = 0u; i < 10u; ++i) {
		y = doubleround(y);
	}
	std::transform(x.begin(), x.end(), y.begin(), y.begin(), std::plus<word>{});
	return y;
}


byte_block_64 salsa20_hash(byte_block_64 x) noexcept {
	auto tmp = word_block_16{};
	for (auto i = 0u; i < x.size(); i +=4) {
		tmp[i/4] = read_le_word(x.data() + i);
	}
	tmp = salsa20_hash_words(tmp);
	for (auto i = 0u; i < x.size(); i +=4) {
		const auto w = word{tmp[i/4]};
		x[i  ] = inv_little_endian<0>(w);
		x[i+1] = inv_little_endian<1>(w);
		x[i+2] = inv_little_endian<2>(w);
		x[i+3] = inv_little_endian<3>(w);
	}
	return x;
}


byte_block_64 salsa20_expand(const byte_block_32& k, const byte_block_16& n) noexcept {
	auto input_block = byte_block_64{};
	input_block[ 0] = 101; input_block[ 1] = 120; input_block[ 2] = 112; input_block[ 3] =  97;
	input_block[20] = 110; input_block[21] = 100; input_block[22] =  32; input_block[23] =  51;
	input_block[40] =  50; input_block[41] =  45; input_block[42] =  98; input_block[43] = 121;
	input_block[60] = 116; input_block[61] = 101; input_block[62] =  32; input_block[63] = 107;
	std::copy_n(k.begin(),      16, input_block.begin() +  4);
	std::copy_n(n.begin(),      16, input_block.begin() + 24);
	std::copy_n(k.begin() + 16, 16, input_block.begin() + 44);
	return salsa20_hash(input_block);
}


byte_block_64 salsa20_expand(const byte_block_16& k, const byte_block_16& n) noexcept {
	auto input_block = byte_block_64{};
	input_block[ 0] = 101; input_block[ 1] = 120; input_block[ 2] = 112; input_block[ 3] =  97;
	input_block[20] = 110; input_block[21] = 100; input_block[22] =  32; input_block[23] =  49;
	input_block[40] =  54; input_block[41] =  45; input_block[42] =  98; input_block[43] = 121;
	input_block[60] = 116; input_block[61] = 101; input_block[62] =  32; input_block[63] = 107;
	std::copy_n(k.begin(), 16, input_block.begin() +  4);
	std::copy_n(n.begin(), 16, input_block.begin() + 24);
	std::copy_n(k.begin(), 16, input_block.begin() + 44);
	return salsa20_hash(input_block);
}


} // namespace detail
} // namespace salsa20
} // namespace mcp

