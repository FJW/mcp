#pragma once

#include "mcp/salsa20.hpp"

#include <algorithm>
#include <cstring>
#include <functional>

#include "utils.hpp"

namespace mcp {
namespace salsa20 {
namespace detail {

using namespace ::mcp::detail;

constexpr auto blocksize = 64u;

word_block_4 quarterround(word y0, word y1, word y2, word y3) noexcept;
word_block_16 rowround(word_block_16 y) noexcept;
word_block_16 columnround(word_block_16 y) noexcept;
word_block_16 doubleround(word_block_16 y) noexcept;

word_block_16 salsa20_hash_words(const word_block_16& x) noexcept;

byte_block_64 salsa20_hash(byte_block_64 x) noexcept;

byte_block_64 salsa20_expand(const byte_block_32& k, const byte_block_16& n) noexcept;
byte_block_64 salsa20_expand(const byte_block_16& k, const byte_block_16& n) noexcept;

template<typename Key>
void salsa20_stream(const Key& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept {
	auto ctr = byte_block_16{};
	std::copy(nounce.begin(), nounce.end(), ctr.begin());
	auto& i = *(reinterpret_cast<std::uint64_t*>(ctr.data() + 8));
	i = 0;
	while (n > blocksize) {
		const auto streamblock = detail::salsa20_expand(key, ctr);
		std::copy_n(streamblock.begin(), blocksize, out);
		++i;
		n -= blocksize;
		out += blocksize;
	}
	const auto streamblock = detail::salsa20_expand(key, ctr);
	std::copy_n(streamblock.begin(), n, out);
}

template<typename Key>
void salsa20(const Key& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept {
	auto ctr = byte_block_16{};
	std::copy(nounce.begin(), nounce.end(), ctr.begin());
	auto& i = *(reinterpret_cast<std::uint64_t*>(ctr.data() + 8));
	i = 0;
	while (n > blocksize) {
		const auto streamblock = detail::salsa20_expand(key, ctr);
		std::transform(in, in +n, streamblock.begin(), out, std::bit_xor<byte>{});
		++i;
		n -= blocksize;
		in += blocksize;
		out += blocksize;
	}
	const auto streamblock = detail::salsa20_expand(key, ctr);
	std::transform(in, in +n, streamblock.begin(), out, std::bit_xor<byte>{});
}


} // namespace detail
} // namespace salsa20
} // namespace mcp



