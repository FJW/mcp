
#include "utils.hpp"

#include <cassert>

namespace mcp {
namespace detail {


dword read_le_dword(const byte* ptr) {
	return dword{
		  dword{ptr[0]} <<  0u
		| dword{ptr[1]} <<  8u
		| dword{ptr[2]} << 16u
		| dword{ptr[3]} << 24u
		| dword{ptr[4]} << 32u
		| dword{ptr[5]} << 40u
		| dword{ptr[6]} << 48u
		| dword{ptr[7]} << 56u
	};
}


dword read_be_dword(const byte* ptr) {
	return dword{
		  dword{ptr[0]} << 56u
		| dword{ptr[1]} << 48u
		| dword{ptr[2]} << 40u
		| dword{ptr[3]} << 32u
		| dword{ptr[4]} << 24u
		| dword{ptr[5]} << 16u
		| dword{ptr[6]} <<  8u
		| dword{ptr[7]} <<  0u
	};
}


void write_le(byte* ptr, dword w) {
	ptr[0] = static_cast<byte>(w);
	ptr[1] = static_cast<byte>(w >> 8u);
	ptr[2] = static_cast<byte>(w >> 16u);
	ptr[3] = static_cast<byte>(w >> 24u);
	ptr[4] = static_cast<byte>(w >> 32u);
	ptr[5] = static_cast<byte>(w >> 40u);
	ptr[6] = static_cast<byte>(w >> 48u);
	ptr[7] = static_cast<byte>(w >> 56u);
}


void write_be(byte* ptr, dword w) {
	ptr[7] = static_cast<byte>(w);
	ptr[6] = static_cast<byte>(w >> 8u);
	ptr[5] = static_cast<byte>(w >> 16u);
	ptr[4] = static_cast<byte>(w >> 24u);
	ptr[3] = static_cast<byte>(w >> 32u);
	ptr[2] = static_cast<byte>(w >> 40u);
	ptr[1] = static_cast<byte>(w >> 48u);
	ptr[0] = static_cast<byte>(w >> 56u);
}

dword read_be_dword_n(const byte* bytes, unsigned n) {
	assert(n < 8u);
	auto res = dword{};
	for (auto i = 0u; i < n; ++i) {
		res <<= 8u;
		res += bytes[i];
	}
	res <<= 8u * (sizeof(dword) - n);
	return res;
}


} // namespace detail
} // namespace mcp


