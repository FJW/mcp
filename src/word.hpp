#pragma once

#include <cstdint>
#include <tuple>

#include "mcp/core.hpp"

namespace mcp {
namespace detail {

using word = std::uint32_t;
using word_block_4  = std::tuple<word, word, word, word>;
using word_block_8 = std::array<word, 8>;
using word_block_16 = std::array<word, 16>;


template<word N>
word rshift(word x) {
	static_assert(N < 32, "");
	return x >> N;
}

template<word N>
word rrot(word x) {
	static_assert(0 < N and N < 32, "");
	return (x >> N) bitor (x << (32 - N));
}

template<word N>
word lrot(word x) {
	static_assert(0 < N and N < 32, "");
	return (x << N) bitor (x >> (32 - N));
}
template<>
inline word lrot<0>(word x) {
	return x;
}


word read_le_word(const byte* ptr);
word read_be_word(const byte* ptr);
void write_le(byte* ptr, word w);
void write_be(byte* ptr, word w);

// only reads n bytes, msb = *ptr
word read_be_word_n(const byte* ptr, unsigned n);


template<word B>
byte inv_little_endian(word w) {
	static_assert(0 <= B and B < 4, "");
	return static_cast<byte>((w >> word{B * 8}) bitand word{0xff});
}



} // namespace detail
} // namespace mcp


