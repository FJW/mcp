#pragma once

#include <array>
#include <cstdint>
#include <string>

#include "core.hpp"

namespace mcp {
namespace salsa20 {

namespace lowlevel {
void salsa20_stream(const key_128& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept;
void salsa20_stream(const key_256& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept;
void salsa20(const key_128& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept;
void salsa20(const key_256& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept;
} // namespace lowlevel

inline namespace highlevel {
std::string salsa20(const key_128& key, const nounce_64& nounce, const std::string& m);
std::string salsa20(const key_256& key, const nounce_64& nounce, const std::string& m);

std::string salsa20_enc(const key_128& key, const std::string& m);
std::string salsa20_enc(const key_256& key, const std::string& m);

std::string salsa20_dec(const key_128& key, const std::string& m);
std::string salsa20_dec(const key_256& key, const std::string& m);
} // namespace highlevel

} // namespace salsa20
} // namespace mcp

