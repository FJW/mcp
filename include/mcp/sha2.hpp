#pragma once

#include "core.hpp"

#include <string>

namespace mcp {
namespace sha2 {

struct sha2_256_hash : hash_256 {
	using hash_256::hash_256;
};
struct sha2_384_hash : hash_384 {
	using hash_384::hash_384;
};
struct sha2_512_hash : hash_512 {
	using hash_512::hash_512;
};

namespace lowlevel {
sha2_256_hash sha2_256(const byte* ptr, std::size_t length);
sha2_256_hash sha2_256(const char* ptr, std::size_t length);
sha2_384_hash sha2_384(const byte* ptr, std::size_t length);
sha2_384_hash sha2_384(const char* ptr, std::size_t length);
sha2_512_hash sha2_512(const byte* ptr, std::size_t length);
sha2_512_hash sha2_512(const char* ptr, std::size_t length);
}

inline namespace highlevel {
sha2_256_hash sha2_256(const std::string& m);
sha2_384_hash sha2_384(const std::string& m);
sha2_512_hash sha2_512(const std::string& m);
}

} // namespace sha2
} // namespace mcp
