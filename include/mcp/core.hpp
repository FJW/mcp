#pragma once

#include <array>
#include <cassert>
#include <cstdint>
#include <initializer_list>
#include <algorithm>
#include <iostream>
#include <iomanip>


#include "errors.hpp"


namespace mcp {

using byte = std::uint8_t;

void read_hex(byte* out, std::size_t n, const char* str);

struct from_hex_t {
	std::string str;
};
inline from_hex_t from_hex(std::string str) {
	return from_hex_t{std::move(str)};
}

struct from_raw_t {
	const byte* data;
};
inline from_raw_t from_raw(const byte* data) {
	return from_raw_t{data};
}
inline from_raw_t from_raw(const char* data) {
	return from_raw_t{reinterpret_cast<const byte*>(data)};
}

template<unsigned Bytes>
class byte_block {
public:
	byte_block() {std::fill(m_data.begin(), m_data.end(), 0);}

	
	byte_block(const std::array<byte, Bytes>& data): m_data{data} {}
	

	// soon to be deprecated:
	byte_block(std::initializer_list<byte> bytes) {
		if (bytes.size() != Bytes) {
			throw input_length_error{"byte_block{init_list}"};
		}
		std::copy(bytes.begin(), bytes.end(), m_data.begin());
	}


	byte_block(from_hex_t hex) {
		if (hex.str.size() != Bytes*2u) {
		}
		read_hex(data(), Bytes, hex.str.c_str());
	}
	

	byte_block(from_raw_t raw) {
		std::copy_n(raw.data, Bytes, data());
	}


	byte_block(const byte* ptr) {
		std::copy_n(ptr, Bytes, data());
	}

	
	byte* data() {return m_data.data();}
	const byte* data() const {return m_data.data();}


	byte* begin() {return data();}
	byte* end() {return data() + Bytes;}
	

	const byte* begin() const {return data();}
	const byte* end() const {return data() + Bytes;}


	byte& operator[](unsigned index) {return m_data[index];}
	const byte& operator[](unsigned index) const {return m_data[index];}


	constexpr unsigned size() const {return Bytes;}
	

	friend bool operator==(const byte_block<Bytes>& lhs, const byte_block<Bytes>& rhs) {
		return lhs.m_data == rhs.m_data;
	}

private:
	std::array<byte, Bytes> m_data;
};


template<unsigned Bytes>
bool operator!=(const byte_block<Bytes>& lhs, const byte_block<Bytes>& rhs) {return !(lhs == rhs);}


template<unsigned Bytes>
std::ostream& operator<<(std::ostream& s, const byte_block<Bytes>& block) {
	auto flags = std::ios::fmtflags(s.flags());
	s << std::hex << std::setfill('0');
	for (auto b: block) {
		s << std::setw(2) << +b;
	}
	s.flags(flags);
	return s;
}

using byte_block_64 = byte_block<64>;
using byte_block_48 = byte_block<48>;
using byte_block_24 = byte_block<24>;
using byte_block_32 = byte_block<32>;
using byte_block_16 = byte_block<16>;
using byte_block_8  = byte_block< 8>;


struct key_128: byte_block_16 {
	using byte_block_16::byte_block_16;
};
struct key_192: byte_block_24 {
	using byte_block_24::byte_block_24;
};
struct key_256: byte_block_32 {
	using byte_block_32::byte_block_32;
};

struct nounce_64: byte_block_8 {
	using byte_block_8::byte_block_8;
};
struct nounce_128: byte_block_16 {
	using byte_block_16::byte_block_16;
};
struct nounce_192: byte_block_24 {
	using byte_block_24::byte_block_24;
};
struct nounce_256: byte_block_32 {
	using byte_block_32::byte_block_32;
};

struct hash_256: byte_block_32 {
	using byte_block_32::byte_block_32;
};
struct hash_384: byte_block_48 {
	using byte_block_48::byte_block_48;
};
struct hash_512: byte_block_64 {
	using byte_block_64::byte_block_64;
};


} // namespace mcp

