#pragma once

#include <array>
#include <cstdint>
#include <string>

#include "core.hpp"

namespace mcp {
namespace chacha20 {

namespace lowlevel {
void chacha20_stream(const key_128& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept;
void chacha20_stream(const key_256& key, const nounce_64& nounce, byte* out, std::size_t n) noexcept;
void chacha20(const key_128& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept;
void chacha20(const key_256& key, const nounce_64& nounce, const byte* in, std::size_t n, byte* out) noexcept;
} // namespace lowlevel

inline namespace highlevel {
std::string chacha20(const key_128& key, const nounce_64& nounce, const std::string& m);
std::string chacha20(const key_256& key, const nounce_64& nounce, const std::string& m);

std::string chacha20_enc(const key_128& key, const std::string& m);
std::string chacha20_enc(const key_256& key, const std::string& m);

std::string chacha20_dec(const key_128& key, const std::string& m);
std::string chacha20_dec(const key_256& key, const std::string& m);
} // namespace highlevel


namespace lowlevel {
void xchacha20_stream(const key_128& key, const nounce_192& nounce, byte* out, std::size_t n) noexcept;
void xchacha20_stream(const key_256& key, const nounce_192& nounce, byte* out, std::size_t n) noexcept;
void xchacha20(const key_128& key, const nounce_192& nounce, const byte* in, std::size_t n, byte* out) noexcept;
void xchacha20(const key_256& key, const nounce_192& nounce, const byte* in, std::size_t n, byte* out) noexcept;
} // namespace lowlevel

inline namespace highlevel {
std::string xchacha20(const key_128& key, const nounce_192& nounce, const std::string& m);
std::string xchacha20(const key_256& key, const nounce_192& nounce, const std::string& m);

std::string xchacha20_enc(const key_128& key, const std::string& m);
std::string xchacha20_enc(const key_256& key, const std::string& m);

std::string xchacha20_dec(const key_128& key, const std::string& m);
std::string xchacha20_dec(const key_256& key, const std::string& m);
} // namespace highlevel

} // namespace chacha20
} // namespace mcp

