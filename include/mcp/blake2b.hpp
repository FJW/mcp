#pragma once

#include "core.hpp"

#include <string>

namespace mcp {
namespace blake2b {

struct blake2b_256_hash : hash_256 {
	using hash_256::hash_256;
};
struct blake2b_384_hash : hash_384 {
	using hash_384::hash_384;
};
struct blake2b_512_hash : hash_512 {
	using hash_512::hash_512;
};

namespace lowlevel {
blake2b_256_hash blake2b_256(const byte* ptr, std::size_t length);
blake2b_256_hash blake2b_256(const char* ptr, std::size_t length);
blake2b_384_hash blake2b_384(const byte* ptr, std::size_t length);
blake2b_384_hash blake2b_384(const char* ptr, std::size_t length);
blake2b_512_hash blake2b_512(const byte* ptr, std::size_t length);
blake2b_512_hash blake2b_512(const char* ptr, std::size_t length);
}

inline namespace highlevel {
blake2b_256_hash blake2b_256(const std::string& m);
blake2b_384_hash blake2b_384(const std::string& m);
blake2b_512_hash blake2b_512(const std::string& m);
}

} // namespace blake2b
} // namespace mcp
