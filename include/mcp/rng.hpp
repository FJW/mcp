#pragma once

#include "core.hpp"

namespace mcp {

void random_fill(byte* b, std::size_t n);

key_128 gen_key_128();
key_192 gen_key_192();
key_256 gen_key_256();

nounce_64 gen_nounce_64();
nounce_128 gen_nounce_128();
nounce_192 gen_nounce_192();
nounce_256 gen_nounce_256();

} // namespace mcp
