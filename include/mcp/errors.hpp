#pragma once

#include <stdexcept>

namespace mcp {
	class parsing_error: public std::runtime_error {
		using std::runtime_error::runtime_error;
	};
	class input_length_error: public std::runtime_error {
		using std::runtime_error::runtime_error;
	};
}
